import cPickle
import gzip
import numpy as np
import os
import shutil
import zipfile
import sys
import nn as nn


'''from expand_mnist import *
training_data, validation_data, test_data = load_mnist_data_wrapper()
print "training_data.shape:"
print training_data[0][1][:, 0]'''

my_zip = "../sample_data/4-days-of-stocks.zip"

def giveAnswers(table):
	# assess data
	answers = []
	cng = np.zeros((len(table) - 1))

	good = 0

	for a in range(len(table) - 1):
		#print table[a]
		answers.append(np.zeros((1, 1)))
		if table[a, 1] != 0.0:
			for b in range(a+1, len(table) - 1 - a):
				#print ": %i -> %i" % (a, b)
				#print table[a]
				#print table[b]
				if table[b, 1] / table[a, 1] > 1.01:
					#print "%i -> %i\n" % (a, b)
					answers[a][0, 0] = 1.0
					good += 1
					break
	#print answers.shape
	#answers = answers.transpose()
	#print answers.shape
	print "good %i/%i" % (good, len(data))
	return answers

print nn
net = nn.nn([5, 100, 1])

filecount = 0

with zipfile.ZipFile(my_zip) as zip_file:
	for member in zip_file.namelist():
		filename = os.path.basename(member)
		if not filename:
			continue

		with zip_file.open(member) as myfile:
			filecount += 1
			data = myfile.read().splitlines()
			header = data[0].split(", ")
			hLabels = {}
			for i, key in zip(range(len(header)), header):
				hLabels[key] = i
			#print hLabels
			#exit(1)
			attributes = ["time", "last trade", "percent change", "pos", "soK14"]
			table = np.zeros((len(data) - 1, len(attributes)))
			data.pop(0)
			#print range(len(data) - 1)
			for i in range(len(data) - 1):
				line = data[i].split(", ")
				#print line
				try:
					for j, key in zip(range(len(attributes)), attributes):
						#print key
						#print float(line[hLabels["last trade"]])
						#exit(1)
						#break
						if key == "percent change":
							entry = (float(line[hLabels['last trade']]) / float(line[hLabels['open']])) - 1.0
						else:
							entry = line[hLabels[key]]
						#print entry

						if key == "time":
							arr = entry.split(":")
							if len(arr) > 1:
								val = (int(arr[0]) * 3600) + (int(arr[1]) * 60) + int(arr[2])
								val -= (30600 - 3600) # 8.5 hours
								val = float(val) / 25200.0
								#print "-> %f" % (val)
								table[i, j] = val
						elif key == "pos":
							if entry == "B":
								table[i, j] = 1.0
							else:
								table[i, j] = 0.0
						else:
							#print entry
							if type(entry) is float:
								table[i, j] = entry
							else:
								table[i, j] = float(entry)
				except:
					#print "Unexpected error:", sys.exc_info()[0]
					pass
					

			#print table
			#table = table[::-1]
			#giveAnswers(table)
			x = table
			y = giveAnswers(table)
			#dataset = zip(x, y)
			dataset = []
			for i in range(0, len(y)):
				dataset.append((x[i].reshape((5, 1)), y[i]))
			#print "dataset.shape"
			#print dataset[0]
			if filecount % 10 == 0:
				correct = net.evaluate(dataset)
				print "%i/%i (%f)" % (correct, len(dataset), (float(correct) / float(len(dataset))) * 100.0)
			else:
				net.SGD(dataset, 3, 30, 0.05)



#def load_data():
	#f = gzip.open('../sample_data/4-days-of-stocks.gz', 'rb')
	#print f
	#data = cPickle.load(f)
	#print data[0]
	#training_data, validation_data, test_data = cPickle.load(f)
	#f.close()
	#return (training_data, validation_data, test_data)

#load_data()
