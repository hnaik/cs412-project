#!/bin/python
import numpy as np
import math

class LogisticRegression:
	def __init__(self, outputs, features):
		# vector
		self.biases = np.random.uniform(low=-1.0, high=1.0, size=(outputs, 1))
		self.biases_grad = np.zeros((outputs, 1))
		# matrix output x feature (y, x) (row, col)
		self.weights = np.random.uniform(low=-1.0, high=1.0, size=(outputs, features))
		#print self.weights
		self.weights_grad = np.zeros((outputs, features))
		self.outputs = outputs
		self.features = features

	# where x is a vector input of features
	# returns the probability of each class
	def probabilty(self, x):
		#eTerm = np.zeros((self.outputs, 1))
		#eTerm = [eTerm + np.exp(bias_k + np.dot(self.weights, x)) for bias_k in self.biases]
		eTerm = np.exp(self.biases + np.dot(self.weights, x))
		return eTerm / (1.0 + eTerm) # np.sum(np.dot(x[:, 1], self.weights))

	def computeGradient(self, x, y):
		print y
		prob = self.probabilty(x)
		self.biases_grad = y - prob
		weights_partial = np.zeros((self.outputs, self.features))
		# number of weights
		# y_k * x_i term for all k
		multProduct = np.sum(np.dot(x, y.transpose()), axis=1)
		expSumTerm = np.zeros((self.outputs, 1))
		print "multProduct shape:"
		print multProduct.shape

		hammard = np.vectorize(lambda t:  np.sum(np.exp(t + self.biases) / (1.0 + np.exp(t + self.biases))))
		lastTerm = np.sum(hammard(np.dot(self.weights, x)))
		print "shape weights * x"
		print np.dot(self.weights, x).shape

		print self.weights_grad.shape
		self.weights_grad = multProduct - np.dot(x, lastTerm)
		print self.weights_grad.shape

		return (self.biases_grad, self.weights_grad)

		#for i in range(len(self.weights_grad)):
			# number of biases & outputs
		#	for k in range(len(self.biases)): 
		#		weights_partial[k, i] = (y[k] * x[i]) - (x[i] * prob[k])

		#	self.weights_grad[i] = np.sum(weights_partial[i, :])

		#return (self.biases_grad, self.weights_grad)

	# xs and ys are now matricies
	def gradientDecent(self, xs, ys, learningRate=0.1):
		for i in range(200):
			for d in range(len(xs[0])): # should be len + 1, hm
				#print "range d %i" % d
				self.computeGradient(xs[:, d], ys[:, d])
				self.weights += self.weights_grad * learningRate
				self.biases += self.biases_grad * learningRate

	# where tups are xs and ys in a tuple array [(x, y)]
	def stocasticGradientDecent(self, tups, validation_data=None, learningRate=0.1, epoch=400, batchSize=20):
		# run each batch several times
		for e in range(epoch):
			np.random.shuffle(tups)
			batchNumber = 0
			batches = self.divideList(tups, batchSize)
			bn = 0
			for batch in batches:
				print "%i of %i batches" % (bn, len(tups) / batchSize)
				bn += 1
				batch_bias_grad = np.zeros((self.outputs, 1))
				batch_weights_grad = np.zeros((self.outputs, self.features))
				total = 0.0
				for tup in batch:
					b_g, w_g = self.computeGradient(np.matrix(tup[0]), np.matrix(tup[1]))
					batch_bias_grad += b_g
					#batch_bias_grad += self.biases
					batch_weights_grad += w_g
					#batch_weights_grad += self.weights
					total += 1.0
				self.weights -= batch_weights_grad * (learningRate / total)
				self.biases -= batch_bias_grad * (learningRate / total)


				if validation_data != None and bn % 100 == 0:
					print "   batch %i" % bn
					print "   total accuracy so far: %f" % self.totalAccuracy(validation_data)
					batchNumber += 1


			if validation_data != None:
				print "   epoch %i" % batchNumber
				print "   total accuracy so far: %f" % self.totalAccuracy(validation_data)
				batchNumber += 1

	def totalAccuracy(self, data):
		right = 0.0
		for i in range(len(data)):
			correct = self.probabilty(np.matrix(data[i][0])).argmax(axis=0)
			guess = max(np.matrix(data[i][1]))
			#print "%i -> %i" % (correct, guess)
			if correct == guess:
				right += 1.0
				#print "  :)"
		return (right / float(len(data))) * 100.0

	def divideList(self, l, n):
		for i in range(0, len(l), n):
			yield l[i:i + n]

