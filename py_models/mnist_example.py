#!/bin/python
import numpy as np
from expand_mnist import *
from logistic_regression import LogisticRegression
import math


def simpleExample():
	# define new instance 2 output variables, 3 input variables
	lg = LogisticRegression(2, 3)

	# should have random outputs
	print "before computation:"
	print lg.probabilty(np.matrix("0; 0; 0"))

	# naive way to compute gradient decent but works okay for this little example
	lg.gradientDecent(np.matrix("0 1; 0 1; 0 1"), np.matrix("1 0; 0 1"))

	# should have a sort correct answer with rouding
	print "after computation:"
	print lg.probabilty(np.matrix("1; 1; 1"))
	print lg.probabilty(np.matrix("0; 0; 0"))
	# you can see from this mini example that it does indeed learn the values 
	# pretty close. 

def mnistExample():
	training_data, validation_data, test_data = load_mnist_data_wrapper()

	# outputs 0-9 guess from a 28x28 image
	model = LogisticRegression(10, 28*28)
	#print np.matrix(training_data[0][0])

	train_length = len(training_data)

	# should probably mini batch gradient for best results, naive attempt
	'''print "begin training"
	for i in range(train_length):
		if i % 10 == 0:
			print "   training %i" % i
			print "   total accuracy so far: %f" % totalAccuracy(model, validation_data)
		#print np.matrix(training_data[i][0])
		#model.gradientDecent(np.matrix(training_data[i][0]), np.matrix(training_data[i][1]))'''
	print "Starting stocasticGradientDecent"
	model.stocasticGradientDecent(training_data, test_data, batchSize=10, epoch=30, learningRate=3.0)
	print "Done with stocasticGradientDecent"

#simpleExample()
mnistExample()


weights = np.matrix("1 2 3; 4 5 6")
biases = np.matrix("2; 4")
x = np.matrix("1; 2; 3")

outputs = 2

expSumTerm = np.zeros((outputs, 1))
print np.dot(weights, x)
print "~~"
print biases
print "!!"
#print np.dot(np.dot(weights, x), biases.transpose())
hammard = lambda t: np.sum(np.exp(t + biases))
vfunc = np.vectorize(hammard)
print vfunc(np.dot(weights, x))
print "!!"
expSumTerm = [expSumTerm + np.dot(weights, x) + bias_k[0][0, 0] for bias_k in biases]
print expSumTerm
'''print w
print x
print "product:"
print np.dot(w, x) + 5 - np.matrix("19; 37")
print "test:"
print np.sum(np.dot(np.matrix("1; 2; 3"), np.matrix("4; 5").transpose()), axis=1)
'''