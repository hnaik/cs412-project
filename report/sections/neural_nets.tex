\lstset{frame=tb,
	aboveskip=3mm,
	belowskip=3mm,
	showstringspaces=false,
	basicstyle={\tiny\ttfamily},
	numbers=none,
	breaklines=true,
	tabsize=2
}

\subsection{Neural Network}

\subsubsection{Neural Network Framework}
\begin{itemize}
\item Define constraints
\item Clean data for those constraints
\item How to measure performance?
\item Optimize for that performance
\item What were the results?
\item How to improve on what really matters
\item Optimization part II
\item What were the final results?
\item What did we learn?
\end{itemize}

\subsubsection{Define Constraints}
As with any model, it helps to first define constraints. The raw
dataset represents four days of intraday stock information from
preselected assets. These assets are screened for two historical
measures: (1) high volatility and (2) high volume. To reiterate,
volatility allows for enough of a price change to capture a profit and
volume allows price point stability (whether or not the market can
give the prices it shows). Moreover, the model is only interested in
the prices from 8:45 AM to 9:59 AM, because this is usually the time
period of greatest market movement. Within this time range, there are
roughly 150 features corresponding to prices every 30 seconds. The
first 110 are arbitrarily chosen as the input features. The next 20
features are ignored. Then the following 10 are averaged to attain a
target value. The rest of the features are discarded to keep
consistency among examples. See the diagram below for clarification.


\subsubsection{Data Cleaning \& Setup}
After the constraints are defined, the raw data needs to be cleaned
for training. A custom Python script was written to format and
normalize the data. For the sake of brevity, it won\text{'}t be
included here. Most of the relevant processing pertained to how the
data is normalized. Since stocks range in values, price changes are
converted to percentage changes. Therefore, if a stock\text{'}s prices
are {100.0, 101.5, 99.0}, then they are converted to {0.00, 1.50,
-2.46}. The same procedure is applied to the average of the target
features relative to the last input feature (e.g., target percentage
change from input feature 110). As for target value, it needs to be
converted from a continuous output to a discrete classification. A
simple threshold criterion is used. Values of 1 and 2 are given to
values below and above the threshold, respectively. Ties with the
threshold are considered below. Note that {1,2} are used over {0,1}
because Octave does not allow indexing by 0 (our modeling code is
vectorized and maps classification bins by index).

\subsubsection{How to Measure Performance}
Since a non-zero threshold is set, it is unlikely that the number of
positive and negative examples are relatively equal. This is even more
so when such a short time span is considered from input features to
target value (10 minutes). Our chosen threshold value of +1\% roughly
results in a 1:7.7 ratio of positive to negative examples. Therefore,
classification accuracy might be a poor indication of actual
performance and so F-Score will be used as a performance proxy
instead.

\subsubsection{Optimize for F-Score}
There are three primary parameters for our neural network model: the
number of input features, the number of nodes per hidden layer, and
the number of fminunc iterations. Fminunc is a cost minimization
function written by Carl Edward Rasmussen for Octave. Moreover, a
single and double layer neural network are compared. \\\\ First, the
number of input features and the one that results in the highest
F-Score is kept.

\includegraphics[scale=0.4]{assets/features.png}

Next, the same procedure is repeated for the number of nodes per hidden layer.

\includegraphics[scale=0.4]{assets/hidden.png}

Finally, the same is applied to the number of fminunc iterations.

\includegraphics[scale=0.4]{assets/iters.png}

This resulted in the final parameters:\\\\
\begin{tabular}{| c | c c |}
	\hline
	Neural Network Paramater & Single Hidden Layer & Double Hidden Layer \\
	\hline
	Input Layer Size & 90 & 90 \\
	Hidden Layer Size & 10 & 30 \\
	Fminunc Max Iterations & 100 & 100 \\
	Average F-Score & 0.216 & 0.208 \\
	\hline
\end{tabular}


\subsubsection{Where Were the Results?}
Now that F-Score is maximized, how does that actually translate to profitability? A pseudo-Monte Carlo trading simulation was written with the following logic. If an example predicted 2, then it means that the model predicts it will go above the threshold and a trade is initiated. Each trade is worth \$20,000 with \$10 per transaction cost and profit/loss is calculated by the difference between the actual output price and ending input price. The results after 1000 runs:
\begin{lstlisting}
=======SINGLE LAYER=============    =======DOUBLE LAYER=============
=======AVERAGE STATISTICS=======    =======AVERAGE STATISTICS=======
Number of Simulations:  1000        Number of Simulations:  1000
Hidden Layer Size:      10          Hidden Layer Size:      30
Number of Features:     90          Number of Features:     90
fminunc iters:          100         fminunc iters:          100
Accuracy of train set:  0.947638    Accuracy of train set:  0.937964
Accuracy of all 1's:    0.880644    Accuracy of all 1's:    0.880212
Accuracy of test set:   0.814288    Accuracy of test set:   0.809209
Recall:                 0.109324    Recall:                 0.136091
Precision:              0.149291    Precision:              0.166222
F-Score:                0.124253    F-Score:                0.146867
Num of trades (avg):    28          Num of trades (avg):    32
Average profit:         -64.32      Average profit:         -27.06
Average return:         0.997282    Average return:         0.999147
\end{lstlisting}

\subsubsection{What Went Wrong?}
Our model ended up losing money even though F-Score is maximized. A
quick glance at the statistics readout shows a glaring discrepancy
between training and testing accuracies. Such a gap is usually
indicative of a high variance over-fitting issue. A typical remedy for
high variance is to include more examples, but the F-Score is
relatively low at around 0.2 and adding more examples might not be the
best option.

\subsubsection{Optimization Part II}
We went back to basics and looked at the fundamental bias/variance curve. Currently, the model is producing results that are to the right of optimal, so the parameters need to be tweaked to move the results leftward. The same procedure from the \text{"}Optimize for F-Score\text{"} section is repeated but test-set classification accuracy is used instead. This resulted in the final parameters:\\\\
\begin{tabular}{| c | c c |}
	\hline
	Neural Network Paramater & Single Hidden Layer & Double Hidden Layer \\
	\hline
	Input Layer Size & 90 & 90 \\
	Hidden Layer Size & 200 & 200 \\
	Fminunc Max Iterations & 10 & 10 \\
	\hline
\end{tabular}

\subsubsection{Results Part II}
Note that the number of actual trades went significantly down, but the trades made were accurate. 
The average return per trade is 1.012\% and 1.019\% for the double and single layer neural nets, respectively. This is a significant result as roughly a 1\% gain after transaction costs in 10 minutes compounds quickly (Assuming one trade is made per trading day with an initial capital of \$20,000 compounds to \$245,000 after 252 days). 
\begin{lstlisting}
=======SINGLE LAYER=============    =======DOUBLE LAYER=============
=======AVERAGE STATISTICS=======    =======AVERAGE STATISTICS=======
Number of Simulations:  1000        Number of Simulations:  1000
Hidden Layer Size:      200         Hidden Layer Size:      200
Number of Features:     90          Number of Features:     90
fminunc iters:          10          fminunc iters:          10
Accuracy of train set:  0.884880    Accuracy of train set:  0.888448
Accuracy of all 1's:    0.879985    Accuracy of all 1's:    0.880160
Accuracy of test set:   0.877534    Accuracy of test set:   0.875908
Recall:                 0.036162    Recall:                 0.044313
Precision:              0.748092    Precision:              0.604261
F-Score:                0.066429    F-Score:                0.079055
Num of trades (avg):    2           Num of trades (avg):    3
Average profit:         248.16      Average profit:         373.33
Average return:         1.012662    Average return:         1.019060
\end{lstlisting}

\subsubsection{Are We Rich Now?}
Before we move to Wall Street and let our algorithm run amok, further analysis needs to be done on the  distribution of returns. A single black swan event would destroy the trading bankroll and the incremental gains would have to slowly rebuild (It takes roughly 70 consecutive +1\% gains to make back one -50\% loss). Moreover, as the bankroll builds, each trade increases in value. This makes it harder for the trade to enter and exit at the prices shown as a \text{"}bigger fish makes a bigger splash\text{"} and the market adjusts to accommodate the trade. Ideally, the distribution of returns should have a relatively low variance or at least skewed towards the positive end. Only through a more thorough analysis of market conditions can we have full confidence in our neural network model. That being said, it\text{'}s an interesting place to start.
