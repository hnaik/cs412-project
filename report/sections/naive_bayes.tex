\subsection{News Based Prediction Using Naive Bayes}

Staying with the theme of whether it is possible to predict stock
prices based on other related market events, we asked a slightly
different question: ``Is it possible to predict the prices of indices
when there is a news event relating one of the Index
components?''. Our belief was that if spam detection on text messages
was possible then, it should be possible to distinguish good/bad news
and neutral news from a corpus of news feed. Using this information we
wondered if it would be possible to study the effects of such news on
indices. Here were the specific questions we asked:

\begin{enumerate}
\item If we have a news event relating to one of the index components,
  does it infuence the index price?
  
\item If so, what is the window of influence?
  
\item Furthermore, if there is a direct influence on the price, it
  is possible to predict direction?
\end{enumerate}

\subsubsection{Data Curation}

We looked at several of the past works for inspiration and data. One
recent work that stood out was
Ding~\cite{Ding_usingstructured,Ding:2015:DLE:2832415.2832572}. Their
work stated that their dataset was available so we contacted them for
data. Although this dataset was an excellent corpus for doing machine
learning, it would be hard to find free minute-bar price data between
2006 and 2013 to match the time stamps associated with this dataset.
\\\\ Hence we set out to curate our own data for the purpose of this
project. As mentioned in the previous section, we used news feed data
from Nasdaq quote new feed and price data from Google finance.

\subsubsection{Data Preprocessing}
For our study we evaluated \textit{Dow Jones Industrial Average} and
\textit{S\&P 500 Index}. Here is a step by step breakdown of the
entire data curation and preprocessing effort:

\begin{enumerate}
  
\item We first looked at all symbols in a given index and downloaded
  all available news relating to those symbols. Here is an example URL
  for the AAPL stock news:
  \url{http://articlefeeds.nasdaq.com/nasdaq/symbols?symbol=AAPL}.

\item Then we downloaded prices for ETF symbols under study. That
  would be \textit{DIA} in case of Dow Jones Industrial Average and
  \textit{SPY} in case of S\&P 500.

\item Based on timestamps for the news items, compare prices ETF
  prices at time intervals 60, 120, 180, 240 and 300 seconds.

\item Here is how they are labeled based on price movement
  \begin{itemize}
  \item If there was a price increase, label as \textbf{UP(1)}
  \item If there was a price decrease, label as \textbf{DOWN(-1)}
    \item If there was no change, label as \textbf{UNCH(0)}
  \end{itemize}
\end{enumerate}

This data was stored in JSON files. A typical data file was in the
format as shown in figures~\ref{fig:json_short} and
\ref{fig:json_long},. The value \textit{711301} is the unique
identifier for the news item as provided by source and is irrelevant
to the study except to ensuring uniqueness of every data item.

\begin{figure}[H]
\begin{verbatim}
{ "711301": { "240": 1, "120": 1, "300": 1, "text": "thermo fisher
 scientific (tmo) shares cross below 200 dma", "60": 1, "180": 1 },
 ...  }
\end{verbatim}
\caption{Data file for short headline}
\label{fig:json_short}
\end{figure}

\begin{figure}[H]
\begin{verbatim}
{ "711301": { "240": 1, "120": 1, "300": 1, "text": "in trading on
 friday shares of thermo fisher scientific inc symbol tmo crossed
 below their 200 day moving average of 147 62 changing hands as low as
 147 26 per share thermo fisher scientific inc shares are currently
 trading down about 1 on the day the chart below shows the one<img
 src=\"http://feeds.feedburner.com/~r/nasdaq/symbols/~4/6jwwa_7gnco\"
 height=\"1\" width=\"1\" alt=\"\"/>", "60": 1, "180": 1 }, ...  }
\end{verbatim}
\caption{Data file for longer news description}
\label{fig:json_long}
\end{figure}

\subsection{Experiments}
For purpose of training the text data was cleaned using Python NLTK's
stop-word library/corpus. This enabled removal of words like
\textit{the}, \textit{and} which do not typically add value to
prediction.
\\\\
The \textit{Na\"ive Bayes} algorithm was implemented in an approach
similar to the one in HW2 and used to first make binary predictions as
to whether news items had any influence on prices at all. This was
further extended to a multiclass prediction setting inorder to
determine the direction (UP, DOWN) of price movement.
\\
While conducting experiments we had two variables:
\begin{enumerate}
\item \textbf{Window of influence}: We wanted to study the
  influence of news events on price over different lengths of
  time. For our study, we had 60s, 120s, 180s, 240s and 300s
\item \textbf{Length of word segment}: We used bag of words and n-gram
  based settings so as to study if presence of individual words or
  certain phrases made a difference
\end{enumerate}


\subsection{Results}
Our results for the binary class is shown in
figures~\ref{fig:bin_precision},~\ref{fig:bin_recall} and \ref{fig:bin_fscore}
\begin{figure}[H]
  \begin{minipage}{.5\textwidth}
    \centering
  \includegraphics[scale=0.5]{../news/assets/naivebayesbinary-precisions.png}
  \caption{Binary Class (MOVE/UNCH) - Precision}
  \label{fig:bin_precision}
\end{minipage}
  \begin{minipage}{.5\textwidth}
    \centering
  \includegraphics[scale=0.5]{../news/assets/naivebayesbinary-recall.png}
  \caption{Binary Class (MOVE/UNCH) - Recall}
  \label{fig:bin_recall}
  \end{minipage}
\end{figure}

\begin{figure}[H]
  \begin{center}
  \includegraphics[scale=0.5]{../news/assets/naivebayesbinary-fscore.png}
  \caption{Binary Class (MOVE/UNCH) - Recall}
  \label{fig:bin_fscore}
  \end{center}
\end{figure}

Our results for the multiclass is shown in
figures~\ref{fig:multi_precision}~\ref{fig:multi_recall} and \ref{fig:multi_fscore}

\begin{figure}[H]
  \begin{minipage}{.5\textwidth}
    \centering
  \includegraphics[scale=0.5]{../news/assets/naivebayes-precisions.png}
  \caption{Multiclass Class (UP/DOWN/UNCH) - Precision}
  \label{fig:multi_precision}
\end{minipage}
  \begin{minipage}{.5\textwidth}
    \centering
  \includegraphics[scale=0.5]{../news/assets/naivebayes-recall.png}
  \caption{Multiclass Class (UP/DOWN/UNCH) - Recall}
  \label{fig:multi_recall}
  \end{minipage}
\end{figure}

\begin{figure}[H]
  \begin{center}
  \includegraphics[scale=0.5]{../news/assets/naivebayes-fscore.png}
  \caption{Multiclass Class (UP/DOWN/UNCH) - Recall}
  \label{fig:multi_fscore}
  \end{center}
\end{figure}

In both instances of the experiment, precision was high for 60 second
price. That means that the window of influence was around or under the
60 second mark. On the other hand we noticed that f-score was higher
for word segments with larger lengths. We reckon that this was due to
presence of phrases like ``52-Week high'', ``... announces merger'' or
``... low quarterly earnings'' that might have more meaning when used
together than when used separately. For example, phrase like
``52-week'' or ``quarterly earnings'' could go with either ``high'' or
``low'' which could have completely opposite effects. Hence, such
phrases would have more directional prediction ability when used
together than when they stand by themselves.
