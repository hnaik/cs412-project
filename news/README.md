# News Data Curator/Labeler
### Components
- GooglePriceFeed class
- News class
- Added data
  - long: with labels and all fields
  - short: with labels and news title only (lower case & html->text'ified)
- requirements.txt (only tested with python3)

#### Tool Usage
```
usage: ./curate_news.py [-h] [-i INDEX] [-o OUTFILE] [-s SHORTFILE]

Data Updater - 0.1

optional arguments:
  -h, --help            show this help message and exit
  -i INDEX, --index INDEX
  -o OUTFILE, --outfile OUTFILE
  -s SHORTFILE, --shortfile SHORTFILE
```

* Output in JSON format
* Outfile
  * long form: All news fields with labels. Text sections are in raw html form.
* Shortfile
  * short form: title field (text & lower case), change labels
* Both output options can be used together as:
  * ``` ./curate_news.py -o detailed-long.json -s labeled-short.json```
* Sample data generated on 2016-11-23 is in __data__ directory

