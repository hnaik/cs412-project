#!/usr/bin/env python3

import os
import sys

import numpy as np

from argparse import ArgumentParser
from statistics import mean

# Local libraries
from datalib import get_data_loader
from mllib import ModelFactory
from tools import Plotter

def parse_arguments():
    parser = ArgumentParser(sys.argv[0])
    parser.add_argument('-m', '--model', default='random', choices=[
        'random', 'naivebayes', 'naivebayesbinary'], help='[default=random]')
    parser.add_argument('-f', '--file', required=True)
    parser.add_argument('-i', '--interval', default='60')
    parser.add_argument('-n', '--ngram', default=1)
    parser.add_argument('-b', '--bonus', action='store_true')
    parser.add_argument('-r', '--repeat', default=1)

    return parser.parse_args()


def run_experiment(loader, options):
    factory = ModelFactory(options.model)
    model = factory.create(loader.data, options.bonus)
    model.train()
    model.do_predictions()

    return model

def get_stats(options, ngram, woi):
    precisions = []
    recalls = []
    fscores = []
    iterations = int(options.repeat)
    loader = get_data_loader(options.file, str(woi), ngram)
    for i in range(iterations):
        model = run_experiment(loader, options)
        if iterations == 1:
            print(model)
        p, r, f, _ = model.stats
        precisions.append(p)
        recalls.append(r)
        fscores.append(f)
    return mean(precisions), mean(recalls), mean(fscores)

def do_experiments(options):
    intervals = np.array([i * 60 for i in range(1, 6)])
    ngrams = np.array([i for i in range(1, 6)])
    plotter = Plotter(X=intervals, Y=ngrams)

    pfig = os.path.join('assets', '{0}-precisions.png'.format(options.model))
    rfig = os.path.join('assets', '{0}-recall.png'.format(options.model))
    ffig = os.path.join('assets', '{0}-fscore.png'.format(options.model))
    precisions = np.zeros(len(intervals) * len(ngrams)).reshape(
        len(intervals), len(ngrams))
    recalls = np.zeros(len(intervals) * len(ngrams)).reshape(
        len(intervals), len(ngrams))
    fscores = np.zeros(len(intervals) * len(ngrams)).reshape(
        len(intervals), len(ngrams))

    for i, woi in enumerate(intervals):
        for j, ngram in enumerate(ngrams):
            precisions[i][j], recalls[i][j], fscores[i][j] \
                =  get_stats(options, ngram, woi)

    plotter.plot3d(Z=precisions, figname=pfig, rstride=1, cstride=1, color='b',
                   title='Precision')
    plotter.plot3d(Z=recalls, figname=rfig, rstride=1, cstride=2, color='g',
                   title='Recall')
    plotter.plot3d(Z=fscores, figname=ffig, rstride=3, cstride=5, color='r',
                   title='F-Score')


def main():
    options = parse_arguments()
    print('Data file: {}'.format(options.file))
    do_experiments(options)


if __name__ == '__main__':
    main()
