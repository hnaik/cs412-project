import re

from nltk.corpus import stopwords


class TextSanitizer:
    cached_stops = stopwords.words('english')
    discards = set(['--'])

    @staticmethod
    def sanitize_split(s, n):
        s = re.sub(r'<img.*>', '', s)
        s1 = re.sub('[,\?:\']', '', s).rstrip('update').split()
        s2 = TextSanitizer.remove_stop(s1)
        s3 = TextSanitizer.generate_n_grams(s2, n)
        return s3


    @staticmethod
    def remove_stop(l):
        return [TextSanitizer.clean(word) \
                for word in l if word not in TextSanitizer.cached_stops and \
                word not in TextSanitizer.discards and\
                len(word) > 1
        ]

    @staticmethod
    def clean(w):
        return w.strip('(').rstrip(')')

    @staticmethod
    def generate_n_grams(msg, n):
        n_grams = zip(*[msg[i:] for i in range(n)])
        return [' '.join(n_gram) for n_gram in n_grams]
