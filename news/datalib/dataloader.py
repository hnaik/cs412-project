import json
import os
import re

from collections import namedtuple

from .sanitizer import TextSanitizer


MlData = namedtuple('MlData', 'x, y')

class DataLoader(object):
    def __init__(self, filename):
        self._filename = filename
        self._data = []
        self._load()

    @property
    def data(self):
        return self._data

    def _load(self):
        raise NotImplementedError


class JsonLoader(DataLoader):
    def __init__(self, filename, interval, ngram):
        self.__interval = interval
        self.__ngram = ngram
        super(JsonLoader, self).__init__(filename)

    def _load(self):
        with open(self._filename) as f:
            self._parse(json.load(f).values())

    def _parse(self, data):
        print('Interval:{} seconds'.format(self.__interval))

        # to have consistent ordering when shuffle is not desired
        data = sorted(data, key=lambda k: k['text'])
        for point in data:
            self._data.append(MlData(
                TextSanitizer.sanitize_split(point['text'], self.__ngram),
                point[self.__interval]))


def get_data_loader(filename, interval, ngram):
    _, ext = os.path.splitext(filename)
    if ext == '.json':
        return JsonLoader(filename, interval, ngram)
    raise Exception
