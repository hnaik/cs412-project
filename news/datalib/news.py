import csv
import html
import json
import sys
import urllib.request

from dateutil import parser as dateparser
from xml.etree import ElementTree as ET

class News(object):
    url_base = 'http://articlefeeds.nasdaq.com/nasdaq/symbols?symbol='
    intervals = [60, 120, 180, 240, 300]

    def __init__(self, feed):
        self.__news_items = {}
        self.__news_short = {}
        self.__news_long = {}
        self.__feed = feed

    @property
    def all_items(self):
        return self.__news_items

    def annotate(self, symbols):
        for symbol in symbols:
            symbol_news = self.__get(symbol)
            for ticker, payload in symbol_news.items():
                for guid, item in payload.items():
                    print('-' * 116)
                    annotated = self.__annotate_item(item)
                    if not annotated:
                        continue

                    if guid in self.__news_short:
                        sys.stderr.write('ERROR: GUID duplicate. FIXME\n')
                        continue
                    title = {'text': html.unescape(item['title'].lower())}
                    desc = {'text': html.unescape(item['description'].lower())}
                    for interval in News.intervals:
                        interval_s = str(interval)
                        if interval_s not in item:
                            sys.stderr.write('ERROR: %s not found in %s\n' % (
                                interval_s, item))
                            break
                        title.update({interval_s: item[interval_s]})
                        desc.update({interval_s: item[interval_s]})
                    self.__news_short[guid] = title
                    self.__news_long[guid] = desc
            self.__news_items.update(symbol_news)

    def write(self, outfilename, indentation=2):
        News.write_file(outfilename, self.__news_items)
        # with open(outfilename, 'w') as f:
        #     f.write(json.dumps(self.__news_items, indent=indentation))
        # print('Wrote records for {} symbols'.format(len(self.__news_items)))

    def write_short(self, outfilename, indentation=2):
        News.write_file(outfilename, self.__news_short)
        # with open(outfilename, 'w') as f:
        #     f.write(json.dumps(self.__news_short, indent=indentation))
        # print('Wrote {} records'.format(len(self.__news_short)))

    def write_long(self, outfilename, indentation=2):
        News.write_file(outfilename, self.__news_long)
        # with open(outfilename, 'w') as f:
        #     f.write(json.dumps(self.__news_short, indent=indentation))
        # print('Wrote {} records'.format(len(self.__news_short)))

    @staticmethod
    def write_file(fname, dataset, indentation=2):
        with open(fname, 'w') as f:
            f.write(json.dumps(dataset, indent=indentation))
        print('Wrote {0} records to {1}'.format(len(dataset), fname))

    def __annotate_item(self, item):
        dt = dateparser.parse(item['updated'])
        prices = self.__feed.query(dt.timestamp(), News.intervals)
        if len(prices) == 0:
            return False
        print(self.__feed.header)
        print(*prices, sep='\n')
        intervals = [0] + News.intervals
        for i, price in enumerate(prices):
            item.update({str(intervals[i]): price.direction})
        return True

    def __collect(self, symbols):
        for symbol in symbols:
            self.__news_items.update(self.__get(symbol))

    def __get(self, ticker):
        url = News.url_base + ticker
        response = urllib.request.urlopen(url)
        root = ET.fromstring(response.read())
        channels = root.findall('channel')
        feeds = {}
        for channel in channels:
            items = channel.findall('item')
            feeds = {}
            for item in items:
                feeds.update(self.__parse_item(item))
        return {ticker: feeds}

    def __parse_item(self, item):
        feed = {}
        guid_attrib = self.__text(item, 'guid')
        for element in item:
            feed.update(self.__text(item, element.tag))
        return {guid_attrib['guid']: feed}

    def __sanitize_key(self, tag):
        bits = tag.split('}')
        if len(bits) > 1:
            return bits[1]
        return bits[0]

    def __text(self, item, tag):
        try:
            return {self.__sanitize_key(tag):
                    self.__cleanse(item.find(tag).text)}
        except AttributeError:
            sys.stderr.write('tag: {} not found\n'.format(tag))

    def __cleanse(self, s):
        return s.strip().rstrip()
