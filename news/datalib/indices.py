import csv
import os
import sys


class Indices(object):
    table = {
        's&p': 'SP500',
        'dow': 'dowjonesA',
        'nasdaq_composite': 'NASDAQComposite',
        'nasdaq_100': 'nasdaq100',
        'nyse_composite': 'NYSEComposite',
        'ftse_100': 'FTSE100'
    }

    etfs = {
        's&p': 'SPY',
        'dow': 'DIA',
        'nasdaq_composite': 'ONEQ',
        'nasdaq_100': 'QQQ',
        'nyse_composite': 'NYC',
        'ftse_100': 'FTSE'
    }

    def __init__(self):
        self.__symbols = []

    def symbols(self, index):
        try:
            file_realpath = os.path.realpath(__file__)
            file_path = os.path.join(os.path.dirname(file_realpath),
                                     'csvfiles', Indices.table[index] + '.csv')
            with open(os.path.join(file_path)) as f:
                reader = csv.reader(f)
                return [row[0] for row in reader if row[0] != 'ticker']
        except FileNotFoundError as err:
            sys.stderr.write('no CSV file found for {}\n'.format(index))
            print(os.getcwd())


