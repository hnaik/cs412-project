import sys

from bisect import bisect_left, bisect_right
from datetime import datetime
import urllib.request

from statistics import mean

google_fin_preurl = 'https://www.google.com/finance/getprices?' + \
                    'i=60&p=30d&f=d,o,h,l,c,v&df=cpct&q='


class GooglePriceFeed(object):
    def __init__(self, symbol):
        self.__first_ts = 0
        self.__timestamps = []
        self.__prices = self.__get_prices(symbol)

    @property
    def all_prices(self):
        return self.__prices

    @property
    def header(self):
        return PriceTuple.header()

    def query(self, epoch_time, intervals=[]):
        if epoch_time < self.__first_ts:
            print('no records for %u (%s)' % (
                epoch_time, datetime.fromtimestamp(epoch_time)))
            return []

        query_dt = datetime.fromtimestamp(epoch_time)
        print('Queried: %lu, %s' % (epoch_time, query_dt))
        first = self.__query(epoch_time)
        if not first:
            print('no first records for %u (%s)' % (
                epoch_time, datetime.fromtimestamp(epoch_time)))
            return []
        first.seconds = 0        
        rows = [first]
        found_dt = datetime.fromtimestamp(first.ts)
        print('Found  : %lu, %s' % (first.ts, found_dt))
        if found_dt.time().hour >= 15:
            print('after close, not recording')
            return []

        if query_dt.date() !=  found_dt.date():
            print('not same date, not generating')
            return []

        for interval in intervals:
            price = self.__query(epoch_time + interval)
            price.update(seconds=interval, delta=(price.ave - rows[0].ave))
            rows.append(price)
        return rows

    def __query(self, timestamp):
        index = 0
        try:
            index = bisect_right(self.__timestamps, int(timestamp))
            if index:
                return self.__prices[index - 1]
        except IndexError:
            sys.stderr.write('ERROR: index:{0}, size:{1}\n'.format(
                index, len(self.__prices)))

    def __get_prices(self, symbol):
        url = google_fin_preurl + symbol
        response = urllib.request.urlopen(url)
        text = response.read()
        lines = text.split(b'\n')
        begin_time = 0
        prices = []
        for line in lines:
            bits = line.decode('utf-8').split(',')
            if len(bits) < 6 or bits[0] == 'COLUMNS=DATE':
                continue
            row = []
            timestamp = 0
            if bits[0][0] == 'a':                    
                begin_time = int(bits[0][1:])
                timestamp = begin_time
                if self.__first_ts == 0:
                    self.__first_ts = timestamp
            else:
                timestamp = begin_time + (int(bits[0]) * 60)
            row.append(timestamp)
            self.__timestamps.append(timestamp)
            row += [float(cell) for cell in bits[1:]]
            row.append(mean(row[1:5]))
            prices.append(PriceTuple(row))
        return prices


class PriceTuple(object):
    fmt = '%5d | %s | %8.3f | %8.3f | %8.3f | %8.3f | %8.3f | %7d | %8s | %9s '
    unch = ' {:^8s} '.format('UNCH')
    up = ' {:^8s} '.format('UP')
    down = ' {:^8s} '.format('DOWN')

    def __init__(self, r):
        self.__delta = 0
        self.__seconds = 0
        self.ts, self.c, self.h, self.l, self.o, self.vol, self.ave = r

    @property
    def change(self):
        return self.__delta

    @property
    def direction(self):
        if not self.__delta:
            return 0
        return 0 if self.__delta == 0 else 1 if self.__delta > 0 else -1

    def update(self, **kwargs):
        self.__delta = kwargs['delta']        
        self.__seconds = kwargs['seconds']

    def __repr__(self):
        delta = '  ---  ' if not self.__delta else ('%8.3f' % self.__delta)

        if self.__delta is not None:
            direction = PriceTuple.unch if self.__delta == 0 else \
                        PriceTuple.up if self.__delta > 0 else PriceTuple.down
        return PriceTuple.fmt % (
            self.__seconds, str(datetime.fromtimestamp(self.ts)), self.c,
            self.h, self.l, self.o, self.ave, self.vol, delta, direction)

    @staticmethod
    def header():
        f = '{:^4s} | {:^19s} | {:^8s} | {:^8s} | {:^8s} | {:^8s} | {:^8s}' +\
            ' | {:^7s} | {:^8s} | {:^9s}\n'
        s = f.format(
            'After', 'Quote Timestamp', 'Close', 'High','Low', 'Open',
            'OLHC-Ave', 'Vol', 'Change', 'Direction')
        return s + '-' * 116
