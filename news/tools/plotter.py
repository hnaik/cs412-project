from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy


class Plotter(object):
    def __init__(self, **kwargs):
        self._X = kwargs['X'] if 'X' in kwargs else None
        self._Y = kwargs['Y'] if 'Y' in kwargs else None

    def plot3d(self, Z, figname, title, **kwargs):
        print(self._X)
        print(self._Y)
        print(Z)
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_title(title)
        ax.set_xlabel('Window of Influence(sec)')
        ax.set_ylabel('N-Grams')
        ax.set_zlabel(title)
        surf = ax.plot_surface(self._X, self._Y, Z, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False, **kwargs)
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(figname, dpi=fig.dpi, facecolor='w', edgecolor='w',
                    orientation='portrait', bbox_inches='tight', pad_inches=0,
                    frame_on=None, transparent=True)
        # plt.show()

