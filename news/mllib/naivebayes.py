from .model import Model


class NaiveBayesModel(Model):
    def __init__(self, *args):
        self._labels = ['up', 'unch', 'down']
        self._bonus_words = {
            'up': set(['52 week high', 'revenue', 'rise', 'positive', 'solid',
                       'buy', 'merge', 'gain', 'rose']),
            'down': set(['52 week low', 'loss', 'negative', 'lost']),
            'unch': set(),
            }
        super(NaiveBayesModel, self).__init__(*args)
        self._l = {'1': 'up', '0': 'unch', '-1': 'down'}
        self._l_reverse = {'up': 1, 'unch': 0, 'down': -1}
        self._w = {'up': {}, 'unch': {}, 'down': {}}
        self._counts = {'up': 0, 'unch': 0, 'down': 0}
        self._priors = {'up': 0, 'unch': 0, 'down': 0}

    def train(self):
        for sample in self._train:
            label = self._l[str(sample.y)]
            self._counts[label] += 1
            for word in sample.x:
                if word not in self._w[label]:
                    self._w[label].update({word: 0.0})
                factor = self.__factor(word, label)
                self._w[label][word] += factor

        assert(sum(self._counts.values()) == self._n_train)
        for key, value in self._priors.items():
            self._priors[key] = self._counts[key] / self._n_train

    def _do_predictions(self):
        y_true = []
        y_pred = []
        for sample in self._test:
            l = self.__compute_likelihood(sample)
            p = {}
            for cat in self._labels:
                p.update({cat: l[cat] * self._priors[cat]})
            self._y_pred.append(max(p, key=p.get))
            self._y_true.append(self._l[str(sample.y)])

    def __compute_likelihood(self, sample):
        l = {}
        alpha = 0.1
        for cat in self._labels:
            n_k = self._counts[cat]
            l_c = 1
            for word in sample.x:
                if word not in self._w[cat]:
                    continue

                numerator = self._w[cat][word] * self.__factor(
                    word, cat, self._w[cat][word])
                numerator += alpha
                nk_wt = numerator / (n_k + (self._n_train * alpha))
                l_c *= nk_wt
            l.update({cat: l_c})
        return l

    def __factor(self, word, label, factor=1):
        for phrase in self._bonus_words[label]:
            if phrase in word or word in phrase:
                factor += 1.5
        return factor
