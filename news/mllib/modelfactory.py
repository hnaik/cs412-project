from .model import RandomModel
from .naivebayes import NaiveBayesModel
from .naivebayesbinary import NaiveBayesBinaryModel


class ModelFactory(object):
    __models = {
        'random': RandomModel,
        'naivebayes': NaiveBayesModel,
        'naivebayesbinary': NaiveBayesBinaryModel        
    }

    def __init__(self, key):
        self.__key = key


    def create(self, *args):
        return ModelFactory.__models[self.__key](*args)
