from .model import Model

class NaiveBayesBinaryModel(Model):
    def __init__(self, *args):
        super(NaiveBayesBinaryModel, self).__init__(*args)
        self._labels = ['unch', 'move']
        self._w = {'unch': {}, 'move': {}}
        self._counts = {'unch': 0, 'move': 0}
        self._priors = {'unch': 0, 'move': 0}        

    def train(self):
        for sample in self._train:
            label = 'unch' if sample.y == 0 else 'move'
            self._counts[label] += 1
            for word in sample.x:
                if word not in self._w[label]:
                    self._w[label].update({word: 0.0})
                self._w[label][word] += 1
            
        assert(sum(self._counts.values()) == self._n_train)
        for key, value in self._priors.items():
            self._priors[key] = self._counts[key] / self._n_train

    def _do_predictions(self):
        for sample in self._test:
            l = self.__compute_likelihood(sample)
            p = {}

            for cat in self._labels:
                p.update({cat: l[cat] * self._priors[cat]})
            self._y_pred.append(max(p, key=p.get))
            self._y_true.append('unch' if sample.y == 0 else 'move')

    def __compute_likelihood(self, sample):
        l = {}
        alpha = 0.1
        for cat in self._labels:
            denominator = self._counts[cat] + self._n_train * alpha
            l_c = 1
            for word in sample.x:
                if word not in self._w[cat]:
                    continue

                l_c *= (self._w[cat][word] + alpha) / denominator
            l.update({cat: l_c})
        return l
