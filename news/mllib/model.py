from random import random, shuffle

from sklearn.metrics import classification_report, \
    precision_recall_fscore_support

class Model(object):
    def __init__(self, *args):
        self._labs = {-1: 'down', 0: 'unch', 1: 'up'}
        train_fraction = 0.8

        self._n_train = int(train_fraction * len(args[0]))
        data = args[0]
        shuffle(data)
        self._train = data[:self._n_train]
        self._test = data[self._n_train:]

        self._y_true = []
        self._y_pred = []
        self._labels = ['up', 'unch', 'down']

    @property
    def stats(self):
        return self._prfs

    def __repr__(self):
        return classification_report(self._y_true, self._y_pred,
                                     labels=self._labels)

    def train(self):
        raise NotImplementedError

    def do_predictions(self):
        self._do_predictions()
        self._prfs = precision_recall_fscore_support(
            self._y_true, self._y_pred, labels=self._labels, average='weighted')

    def _do_predictions(self):
        raise NotImplementedError        

class RandomModel(Model):
    def __init__(self, *args):
        super(RandomModel, self).__init__(*args)

    def train(self):
        pass

    def _do_predictions(self):
        for record in self._test:
            self._y_pred.append(self._labs[(int(random() * 1000) % 3) - 1]);
            self._y_true.append(self._labs[record.y])
