import numpy as np

from sklearn.metrics import confusion_matrix, classification_report

class ConfusionMatrix(object):
    def __init__(self, *args, **kwargs):
        self._y_true = args[0]
        self._y_pred = args[1]
        self.__m = confusion_matrix(*args, **kwargs)
        self.__labels = kwargs['labels']

    def __repr__(self):
        return classification_report(self._y_true, self._y_pred,
                                     labels=self.__labels)
