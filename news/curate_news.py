#!/usr/bin/env python3

import json
import sys

from argparse import ArgumentParser

# local libraries
from datalib import Indices, News, GooglePriceFeed

desc = 'Data Updater - 0.1'

def parse_options():
    parser = ArgumentParser(sys.argv[0], description=desc)
    parser.add_argument('-i', '--index', default='dow')
    parser.add_argument('-o', '--outfile')
    parser.add_argument('-s', '--shortfile')
    parser.add_argument('-l', '--longfile')

    return parser.parse_args()

def main():
    options = parse_options()
    indices = Indices()
    feed = GooglePriceFeed(Indices.etfs[options.index])

    news = News(feed)
    news.annotate(indices.symbols(options.index))
    if options.outfile:
        news.write(options.outfile)
    if options.shortfile:
        news.write_short(options.shortfile)
    if options.longfile:
        news.write_long(options.longfile)


if __name__ == '__main__':
    main()
